package cliente;

/**
 * Log sencillo que imprime en funcion del nivel de
 * verbose que se le pone.
 * 
 * @author Daniel Iñigo
 *
 */
public class Log {
	private int logLevel;
	
	private Log() {}
	
	public static class SingletonHolder {
		public static final Log instance = new Log();
	}
	
	public static Log getInstance(){
		return SingletonHolder.instance;
	}
	
	/**
	 * Asigna un nivel de log a un Singleton Log 
	 * que ya haya sido instanciado alguna vez.
	 * 
	 * 1 - Normal
	 * 2 - Warnings
	 * 3 - Errors
	 * 
	 * @param logLevel
	 */
	public void setLogLevel(int logLevel){
		this.logLevel = logLevel;
	}
	
	public void e(String text){
		if(this.logLevel > 2){
			System.out.println("***ERROR: " + text);
		}
	}
	
	public void w(String text){
		if(this.logLevel > 1){
			System.out.println("***WARNING: " + text);
		}
	}
	
	public void l(String text){
		if(this.logLevel > 0){
			System.out.println("***LOG: " + text);
		}
	}
}
