package cliente;

import java.net.Socket;
import java.util.ArrayList;

public class UserData {
	// Informacion del usuario del chat.
	private String nick;
	private final ArrayList<String> rooms;
	private String currentRoom;
	// Parametros para conectar con el servidor
	private final String serverURL;
	private final int serverPort;
	private ChatWindowBuilder window;
	private Socket network;
	private Log log;
	
	public UserData(String nick, String serverURL,
			int serverPort, ChatWindowBuilder window, Socket network) {
		super();
		this.nick = nick;
		this.rooms = new ArrayList<String>();
		this.serverURL = serverURL;
		this.serverPort = serverPort;
		this.window = window;
		this.network = network;
		this.log = Log.getInstance();
	}

	/**
	 * Marca la sala que se le pasa como sala actual, y si no est� entre las
	 * salas a las que ya nos hemos unido la a�ade a la lista.
	 * @param room
	 */
	public void join(String room) {
		this.currentRoom = room;
		if( !this.rooms.contains(room)) {
			this.rooms.add(room);
		}
	}

	/**
	 * Elimina la sala de la lista. Si era la �ltima sala visitada se cambia a una cualquiera
	 * @param room
	 */
	public void leave(String room) {
		this.rooms.remove(room);
		if(this.currentRoom.equals(room)){
			if(room.length()>0){
				this.currentRoom = this.rooms.get(0);
			}
		}
	}

	public void nick(String nick) { this.nick = nick; }

	public String getServerURL() { return this.serverURL; }

	public int getServerPort() { return this.serverPort; }

	public String getNick() { return this.nick; }

	/**
	 * Interrumpe los hilos de entrada y salida de datos, cierra el socket
	 * si este se ha instanciado y la ventana.
	 */
	public void quit() {
		try {
			if(network != null) network.close();
			log.l("CERRADO Socket");
		} catch (Exception e ) {
			log.e("no se pudieron cerrar el socket o buffers");
		}
		if(this.window.isVisible()){
			this.window.close();
		}
	}
}
