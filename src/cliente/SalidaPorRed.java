package cliente;

import java.util.concurrent.ArrayBlockingQueue;

/**
 * SalidaPorRed.java
 * 
 * Recoge los comandos creados por la clase EntradaDatos y 
 * se los pasa a la clase Network.
 * Implementa el buffer/array de produccion-consumo
 * 
 * @author Daniel Iñigo
 * @author Hector Martinez
 */
public class SalidaPorRed extends Thread{
	private ArrayBlockingQueue<Message> bufferSalida;
	private final BinaryProtocolWriter output;
	private Log log;
	/**
	 * Instancia el buffer y la clase de red
	 */
	public SalidaPorRed(ArrayBlockingQueue<Message> bufferSalida, BinaryProtocolWriter output){
		this.bufferSalida = bufferSalida;
		this.output = output;
		this.setName("SalidaPorRed");
		this.log = Log.getInstance();
		this.setDaemon(true);
		this.start();
	}

	/**
	 * Lee comandos del buffer y los envía por la red indefinidamente.
	 */
	@Override
	public void run() {
		boolean isClosed = false;
		try {
			while(!isClosed){
				Message outgoingMessage;
				outgoingMessage = bufferSalida.take();
				output.writeMessage(outgoingMessage);
				if( outgoingMessage.getType() == MessageType.QUIT){
					log.l("CERRADO " + this.getName());
					this.interrupt();
					isClosed = true;
				}
			}
		} catch (InterruptedException e) {
			log.w("INTERRUMPIDO " + this.getName());
			return;
		}

	}
}
