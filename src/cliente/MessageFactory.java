package cliente;

public class MessageFactory {

	/**
	 * Devuelve un Mensaje a partir de una linea introducida por pantalla.
	 * Todos los mensajes que crea tienen estado PET porque solo pueden
	 * ser peticiones.
	 * 
	 * @param line
	 * @param nick
	 * @return
	 */
	public static Message create(String line){
		// Esquema de una linea de parametros
		//		[/][m][m][ ][p][;][p][ ][p]
		//		 0  1  2  3  4  5  6  7  8  <-- Index
		//		 1  2  3  4  5  6  7  8  9  <-- Size
		if(!line.startsWith("/")) return null;
		String typeString = line.split(" ")[0].substring(1);
		byte type = MessageType.getValue(typeString);
		byte status = MessageStatus.PET;

		String argsString;
		String[] args = null;
		try{
			argsString  = line.substring(typeString.length()+2);
		}catch(IndexOutOfBoundsException e){
			argsString = null;
		}
		if(argsString != null){
			args = argsString.split(";");
		}
		return new Message(type, status, args);
	}
}
