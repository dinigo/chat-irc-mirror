package cliente;

import java.io.IOException;
import java.util.concurrent.ArrayBlockingQueue;

/**
 * EntradaPorRed.java
 * 
 * Espera a que haya mensajes disponibles, los recibe y los introduce en 
 * un buffer para luego ser mostrados.
 * 
 * @author Daniel Iñigo
 * @author Hector Martinez
 */
public class EntradaPorRed extends Thread{
	private final ArrayBlockingQueue<Message> bufferEntrada;
	private final BinaryProtocolReader input;
	private Log log;
	/**
	 * Constructor de EntradaPorRed
	 * 
	 * @param bufferEntrada
	 * @param network
	 */
	public EntradaPorRed(ArrayBlockingQueue<Message> bufferEntrada, BinaryProtocolReader input){
		this.bufferEntrada = bufferEntrada;
		this.input= input;
		this.setName("EntradaPorRed");
		this.log = Log.getInstance();
		this.setDaemon(true);
		this.start();
	}

	/**
	 * Espera comandos de BinaryProtocolReader y los pone en el buffer de entrada
	 */
	@Override
	public void run() {
		boolean isClosed = false;
		try {
			while(!isClosed){
				Message incomingMessage = getMessage();
				if(incomingMessage != null){
					bufferEntrada.put(incomingMessage);
				}
				if( incomingMessage.getType() == MessageType.QUIT){
					log.l("CERRADO " + this.getName());
					this.interrupt();
					isClosed = true;
				}
			}
		} catch (InterruptedException e) {
			log.w("INTERRUMPIDO " + this.getName());
			return;
		}
	}

	/**
	 * Espera un comando por la red, lo deserializa y lo devuelve.
	 * Si ocurre algun error en la recepción se devuelve un comando = null.
	 * 
	 * @return comando
	 */
	public Message getMessage() {
		Message message;
		try {
			message = input.readMessage();
		} catch (IOException e) {
			message=null;
			e.printStackTrace();
		}
		return message;
	}
}
