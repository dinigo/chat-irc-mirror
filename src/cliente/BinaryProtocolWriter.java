package cliente;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * BinaryProtocolWriter.java
 * Esta clase serializa un mensaje y lo env�a por el stream de salida.
 * @author Daniel I�igo
 * @author Hector Martinez
 */
public class BinaryProtocolWriter {
	private DataOutputStream output;
	private boolean isClosed;
	
	/**
	 * El constructor recibe un OutputStream.
	 * Se utiliza para enviar el mensaje serializado seg�n el protocolo binario
	 * declarado en el hito2.
	 * @param out
	 */
	public BinaryProtocolWriter(OutputStream out){
		this.output = new DataOutputStream(out);
		this.isClosed = false;
	}
	
	/**
	 * Recibe un objeto de la clase Message.java.
	 * Lo serializa con una codificaci�n UTF-8.
	 * Posteriormente se el pasa al OutputStream para que lo env�e por
	 * el stream de salida.
	 * @param message
	 */
	public void writeMessage(Message message) {
		if(this.isClosed) return;
		byte[] messageBytes;
		try {
			messageBytes = MessageSerializer.serialize(message);
		} catch (IOException e) {
			messageBytes = new byte[0];
			System.out.println("ERROR: fallo la serializacion");
			e.printStackTrace();
		}
		try {
			this.output.write(messageBytes);
		} catch (IOException e) {
			System.out.println("ERROR: fallo el envío");
			e.printStackTrace();
		}
	}

	public void close() throws IOException {
		this.output.close();
		this.isClosed = true;
	}

}
