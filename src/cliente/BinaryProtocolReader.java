package cliente;

import java.io.DataInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
	/**
	 * BinaryProtocolReader.java
	 * Esta clase deserializa un mensaje binario en un POJO de tipo Message.java 
	 * construyendo primero un string y posteriormente pasandoselo a MessageFactory.java
	 * para que lo instancie
	 *
	 * @author Daniel I�igo
	 * @author Hector Martinez
	 */
public class BinaryProtocolReader {
	private final DataInputStream input;

	/**
	 * El constructor recibe un InputStream gen�rico, para que valga
	 * tanto para hacer pruebas pas�ndole un ByteArrayInputStream como
	 * para el cliente final, pas�ndole el Socket.getInputStream()
	 *
	 */
	public BinaryProtocolReader(InputStream in) {
		this.input = new DataInputStream(in);
	}

	/**
	 * Lee un parametro del DataInputStream que se le pase.
	 * Lee primero el tamano del parametro en bytes, y despues
	 * obtiene esos bytes y los convierte en una String.
	 * Supone que Está codificado en UTF-8
	 * 
	 * @param input - Entrada de datos
	 * @return parametro - Par
	 * @throws IOException
	 */
	public static String readString(DataInputStream input) throws IOException{
		short sizeLoad = input.readShort();
		byte[] binaryLoad = new byte[sizeLoad];
		input.read(binaryLoad);
		String message = new String(binaryLoad,"UTF-8");
		return message;
	}

	/**
	 * Lee una Respuesta del inputStream y devuelve
	 * su representacion en String
	 * 
	 * @return cadena - Representacion en Sring de un comando
	 * @throws IOException
	 */
	public Message readMessage() throws IOException{
		Message incomingMessage;
		try {
			// Lee los parametros.
			byte status = this.input.readByte();
			byte type		= this.input.readByte();
			short loadSize	= this.input.readShort();
			// Cuidado con los argumentos, puede valer null.
			String[] args = null;
			// Si el mensaje tiene carga lee los argumentos
			if(loadSize > 0){
				short nargs	= this.input.readShort();
				args = new String[nargs];
				for(int i=0; i<args.length; i++){
					args[i] = readString(input);
				}
			}
			// Construye un Message y lo devuelve.
			incomingMessage = new Message(type, status, args);
		} catch (EOFException e) {
			// Si falla por EOF es que el socket se ha cerrado
			incomingMessage =  null;
		}		
		return incomingMessage;
	}

	/**
	 * Cierra el hilo de entrada
	 * @throws IOException
	 */
	public void close() throws IOException {
		this.input.close();
	}
}