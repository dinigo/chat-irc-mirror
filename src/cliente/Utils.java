package cliente;

/**
 * Utils.java
 * 
 * Coleccion de funciones utiles a la hora de realizar operaciones con bytes
 * 
 * @author demil133
 *
 */
public class Utils {

	/**
	 * Imprime un array de bytes de forma reconocible
	 * 
	 * @param array
	 * @return byteArrayRepresentation
	 */
	public static String bytes2string(byte[] array){
		String cadena = "";
		for(byte b: array){
			cadena += String.format("0x%02X", b) + " ";
		}
		return cadena;
	}
	public static String byte2string(byte b){
		return String.format("0x%02X", b);
	}

	/**
	 * Concatena los arrays de bytes para generar uno solo
	 * 
	 * @param arrays
	 * @return concatenado
	 */
	public static byte[] concatenaArrays(byte[] ...arrays){
		int length=0;
		for(byte[] aConcatenar:arrays){
			length+=aConcatenar.length;
		}
		byte[] concatenado=new byte[length];
		int i=0;
		for(byte[] aConcatenar:arrays){
			for(byte valor:aConcatenar){
				concatenado[i]=valor;
				i++;
			}
		}
		return concatenado;
	}

	/**
	 * Convierte un short en un array de dos bytes. Util en el
	 * proceso de serializar y testear los comandos.
	 * 
	 * @param shortToProcess
	 * @return bytesInShort
	 */
	public static byte[] short2bytes(short s){
		byte[] ret = new byte[2];
		ret[1] = (byte)(s & 0xff);
		ret[0] = (byte)((s >> 8) & 0xff);
		return ret;
	}
	
	/**
	 * Convina dos bytes en un short
	 * 
	 * @param b1 - byte alto
	 * @param b2 - byte bajo
	 * @return short
	 */
	public static short bytes2short(byte b1, byte b2) {
		return (short) ((b1 << 8) | (b2 & 0xFF));
	}

	/**
	 * Da formato a un short para imprimirlo y que sea reconocible
	 * 
	 * @param short
	 * @return shortRepresentation
	 */
	public static String short2string(short s){
		return String.format("0x%04X", s);
	}
}
