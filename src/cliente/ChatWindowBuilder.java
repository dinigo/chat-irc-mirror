package cliente;

import java.awt.BorderLayout;
import java.awt.SystemColor;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.util.concurrent.ArrayBlockingQueue;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.BevelBorder;
import javax.swing.text.DefaultCaret;

/**
 * ChatWindowBuilder.java
 * Esta clase se encarga de instanciar la interfaz gr�fica 
 * para introducir por teclado los mensajes que enviar� un usuario a otro de la sala
 * y posteriormente se mostrar� en un �rea de texto los mensajes enviados
 * entre los usuarios de esa sala.
 * @author Daniel Inigo
 * @author Hector Martinez
 */
public class ChatWindowBuilder {

	private JFrame frame;
	private JTextField inputField;
	private JTextArea txtChat;
	private JScrollPane scrollPane;

	private ArrayBlockingQueue<String> bufferSalidaGui;

	public ChatWindowBuilder(ArrayBlockingQueue<String> bufferSalidaGui) {
		this.bufferSalidaGui = bufferSalidaGui;
		initialize();
	}

	/**
	 * Se inicializan los componetes en una cola de eventos seg�n la 
	 * filosof�a de la programaci�n orientada a eventos.
	 */
	private void initialize() {
		// Instancia y configura la ventana
		frame = new JFrame();
		BorderLayout borderLayout = (BorderLayout) frame.getContentPane().getLayout();
		borderLayout.setVgap(3);
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Instancia y configura el scrollPane
		scrollPane = new JScrollPane();
		frame.getContentPane().add(scrollPane, BorderLayout.CENTER);

		// Instancia y configura el area de texto grande donde mostrar
		// la informaci�n del chat
		txtChat = new JTextArea();
		txtChat.setLineWrap(true);
		scrollPane.setViewportView(txtChat);
		txtChat.setEditable(true);
		((DefaultCaret) txtChat.getCaret()).setUpdatePolicy(
				DefaultCaret.ALWAYS_UPDATE);
		txtChat.setBackground(SystemColor.controlHighlight);

		// Crea un panel en la parte inferior para poner la entrada de texto y el
		// bot�n de env�o.
		JPanel panelInferior = new JPanel();
		frame.getContentPane().add(panelInferior, BorderLayout.SOUTH);
		panelInferior.setLayout(new BorderLayout(3, 3));

		// Instancia el area de texto, le pone un listener para cuando se
		// pulsa enter, y lo configura.
		inputField = new JTextField();
		inputField.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		panelInferior.add(inputField, BorderLayout.CENTER);
		inputField.setColumns(10);
		inputField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent key) {
				if (key.getKeyCode() == KeyEvent.VK_ENTER) {
					addTextToChat();
				}
			}
		});

		// Instancia el bot�n de env�o y le pone un listener para cuando se pulsa
		JButton btnEnviar = new JButton("OK");
		btnEnviar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				addTextToChat();
			}
		});
		panelInferior.add(btnEnviar, BorderLayout.EAST);
	}

	/**
	 * Captura el texto que hay en el campo de entrada, lo muestra por campo de
	 * texto de chat y lo a�ade al buffer de salida de texto.
	 * Maneja el extra��simo caso de que se bloqu�e el buffer (la MV debe quedarse
	 * sin memoria).
	 */
	private void addTextToChat() {
		Thread h = new Thread() {
			public void run() {
				// Obtiene la linea
				final String line;
				line = inputField.getText();
				// Dibuja threadsafe
				SwingUtilities.invokeLater(new Runnable() { // otra clase interna
					public void run() {
						txtChat.append(line + "\n");
						inputField.setText("");
					}
				});
				// Mete la linea en el buffer
				try {
					bufferSalidaGui.put(line);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			} 
		};

		// Se arranca el hilo
		h.start();
	}

	/**
	 * Getter que devuelve el area de texto de la sala donde
	 * se muestran los mensajes de los participantes
	 * @return
	 */
	public JTextArea getTxtChat() {
		return txtChat;
	}

	/**
	 * Getter del area de texto donde escribir los mensajes
	 * @return
	 */
	public JTextField getInputField() {
		return inputField;
	}

	/**
	 * Setter que muestra la ventana o no
	 * @param visible
	 */
	public void setVisible(boolean visible) {
		this.frame.setVisible(visible);
	}

	/**
	 * Devuelve true si la ventana de chat es visible
	 * @return
	 */
	public boolean isVisible() {
		return this.frame.isVisible();
	}

	/**
	 * Crea la acci�n de cerrar la ventana y la pone en la cola de eventos
	 */
	public void close() {
		WindowEvent wev = new WindowEvent(this.frame, WindowEvent.WINDOW_CLOSING);
		Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(wev);
		setVisible(false);
	}
}
