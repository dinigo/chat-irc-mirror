package cliente;

/**
 * Message.java
 * OJO almacena los valores del mensaje.
 * No incluye ning�n m�todo que asegure que el n�mero de campos es 
 * el adecuado para cada tipo de mensaje.
 * @author Daniel I�igo
 * @author Hector Martinez
 */
public class Message {

	private final byte type;
	private final byte status;
	private final String[] args;
	
	public Message(byte type, byte status, String[] args){
		this.type = type; 
		this.status = status;
		this.args = args;
	}

	public byte getType() {
		return type;
	}

	public byte getStatus() {
		return status;
	}

	public String[] getArgs() {
		return args;
	}
	
	@Override
	public String toString(){
		String messageString = "Type:" + Utils.byte2string(this.type)
				+  "   Status:" + Utils.byte2string(this.status);
		if(this.args == null) return messageString;
		for(String arg : args){
			messageString += "\n    " + arg;
		}
		return messageString;
	}
	
}
