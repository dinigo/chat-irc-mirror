package cliente;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

public class MessageSerializer {

	/**
	 * A partir de un objeto Message obtiene su representación en bytes de acuerdo
	 * con lo especificado por el hito_2 del proyecto.
	 * 
	 * @param message - Objeto Message.
	 * @return messageBytes - Array de bytes que representan el mensaje.
	 * @throws IOException - Fallo al serializar el mensaje
	 */
	public static byte[] serialize(Message message) throws IOException{
		ByteArrayOutputStream messageBuffer = new ByteArrayOutputStream();
		// Escribe el codigo
		messageBuffer.write(message.getStatus());
		messageBuffer.write(message.getType());
		String[] args = message.getArgs();
		short sizeLoad = (short) getSizeLoad(args);
		// Escribe el peso
		messageBuffer.write(Utils.short2bytes(sizeLoad));
		if(sizeLoad>0){
			// Si hay carga escribe el numero de argumentos
			short nargs = (short) args.length;
			messageBuffer.write(Utils.short2bytes(nargs));
			// A continuacion adjunta el peso y bytes de cada argumento
			for(String arg : args){
				byte[] bytesArg = arg.getBytes("UTF-8");
				short sizeArg = (short) bytesArg.length;
				messageBuffer.write(Utils.short2bytes(sizeArg));
				messageBuffer.write(bytesArg);
			}
		}
		return messageBuffer.toByteArray();
	}

	/**
	 * Calcula los bytes que va a ocupar la carga de un mensaje.
	 */
	public static int getSizeLoad(String[] args) throws UnsupportedEncodingException{
		if(args == null || args.length == 0) return 0;
		int sizeLoad = 2;	// Dos bytes del numero de argumentos
		for(String arg : args){
			sizeLoad += arg.getBytes("UTF-8").length + 2;
		}
		return sizeLoad;
	}

	public static byte[] serialize(String line){
		Message message = MessageFactory.create(line);
		byte[] messageBytes;
		try {
			messageBytes = serialize(message);
		} catch (IOException e) {
			messageBytes = null;
			System.out.println("ERROR: fallo la serialización");
			e.printStackTrace();
		}
		return messageBytes;
	}
}
