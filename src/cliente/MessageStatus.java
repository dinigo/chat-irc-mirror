package cliente;
/**
 * MessageStatus.java
 * Reemplaza el enum que se utilizaba anteriormente 
 * por la facilidad de utilizar directamente shorts
 * y acceder directamente a los valores de cada estado.
 * 
 * Esta clase no requiere una seguridad adicional
 * por lo que sus variables son p�blicas.
 * @author Daniel I�igo
 * @author Hector Martinez
 */
public class MessageStatus {
	// Codigos del estado de las respuestas. Incluye uno para las peticiones
	public final static byte
	INF=0x01,
	OK=0x02,
	ERR=0x03,
	PET=0x00;
}
