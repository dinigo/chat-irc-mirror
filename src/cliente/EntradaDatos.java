package cliente;

import java.util.concurrent.ArrayBlockingQueue;
/**
 * EntradaDatos.java
 * 
 * En este hilo se espera a que el usuario escriba algún mensaje en el teclado,
 * se interpreta lo que el usuario haya escrito y se convierte en un objeto Comando, 
 * se deposita este Comando en el buffer de salida y se vuelve a esperar más texto
 * del usuario.
 * 
 * @author Daniel Iñigo
 * @author Hector Martinez
 */
public class EntradaDatos extends Thread{
	private String nick;
	private ArrayBlockingQueue<Message> bufferSalida;
	private  ArrayBlockingQueue<String> bufferGui;
	private Log log;
	
	/**
	 * Constructor de EntradaDatos.
	 * 
	 * @param nick - Nombre de usuario elegido al inicio
	 * @param bufferSalida - Clase de salida
	 */
	public EntradaDatos(String nick, ArrayBlockingQueue<Message> bufferSalida, ArrayBlockingQueue<String> bufferGui) {
		this.nick = nick;
		this.bufferSalida = bufferSalida;
		this.bufferGui = bufferGui;
		this.setName("EntradaDatos");
		this.log = Log.getInstance();
		this.setDaemon(true);
		this.start();
	}

	/**
	 * Pide Comandos por teclado y los pone en el buffer de salida
	 * hasta que se da la condición de salida
	 */
	@Override
	public void run() {
		boolean isClosed = false;
		try {
			// Registro inicial
			Message outgoingMessage = MessageFactory.create("/NICK " + nick);
			bufferSalida.put(outgoingMessage);
			
			// Bucle de env�o
			while(!isClosed){
				String linea = bufferGui.take();
				outgoingMessage = MessageFactory.create(linea);
				if( outgoingMessage != null ){
					bufferSalida.put(outgoingMessage);
				}
				if( outgoingMessage.getType() == MessageType.QUIT){
					log.l("CERRADO " + this.getName());
					this.interrupt();
					isClosed = true;
				}
			}
		} catch (InterruptedException e) {
			log.w("INTERRUMPIDO " + this.getName());
			return;
		}
	}
}
