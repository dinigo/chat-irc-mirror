package cliente;

import java.awt.EventQueue;

import javax.swing.JTextArea;

/**
 * MessageExecutor.java
 * Recibe un mensaje y ejecuta una acci�n en funci�n del tipo de este.
 * @author Daniel I�igo
 * @author Hector Martinez
 */
public class MessageExecutor {
	private final UserData userData;
	private JTextArea txtChat;

	public MessageExecutor(UserData userData, JTextArea txtChat){
		this.userData = userData;
		this.txtChat = txtChat;
	}

	/**
	 * Se asegura de que el mensaje no es nulo y 
	 * comprueba si el mensaje es .INF, .OK o .ERR 
	 * y solo en el caso de .OK y .ERR los maneja.
	 * 
	 * @param message
	 */
	public void execute(Message message){
		if(message == null) return;

		byte status = message.getStatus();
		byte type = message.getType();
		String[] args = message.getArgs();

		// Maneja los mensajes INF y OK por un lado
		// y los ERR por otro.
		String statusUpdateLine = "";
		switch(status){
		case MessageStatus.INF:
		case MessageStatus.OK:
			statusUpdateLine = updateStatus(type, status, args);
			break;
		case MessageStatus.ERR:
			statusUpdateLine="ERROR: " + args[0];
			break;
		}
		// Encola una actualizaci�n del texto del chat
		final String statusUpdate = statusUpdateLine;
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				if(!statusUpdate.equals("")) {
					txtChat.append(statusUpdate + "\n");
				}
			}
		});
	}

	/**
	 * En funci�n del tipo de mensaje que recibe,
	 * ejecuta la acci�n correspondiente a cada tipo de mensaje
	 * @param type
	 * @param status
	 * @param args
	 */
	private String updateStatus(byte type, byte status, String[] args) {
		String statusUpdateLine = "";
		switch(type){
		case MessageType.MSG:
			statusUpdateLine = args[0] + "|" + args[1] + "> " + args[2];
		case MessageType.JOIN:
			statusUpdateLine = args[0] + " se unio a la sala " + args[1];
			if(status==MessageStatus.OK) userData.join(args[1]);
			break;
		case MessageType.LEAVE:
			statusUpdateLine = args[0] + " se fue de la sala " + args[1];
			if(status==MessageStatus.OK) userData.leave(args[1]);
			break;
		case MessageType.NICK:
			statusUpdateLine = args[0] + " ahora se llama " + args[1];
			if(status==MessageStatus.OK) userData.nick(args[0]);
			break;
		case MessageType.QUIT:
			statusUpdateLine = args[0] + " se fue";
			if(status==MessageStatus.OK) userData.quit();
			break;
		case MessageType.LIST:
			String[] list = args[0].split(";");
			statusUpdateLine="SALAS:";
			for(String room: list) statusUpdateLine="  �" + room;
			break;
		case MessageType.WHO:
			String[] users = args[0].split(";");
			statusUpdateLine="USUARIOS:";
			for(String user : users) statusUpdateLine="  �" + user;
			break;
		case MessageType.HELLO:
			statusUpdateLine=">>>" + args[0];
			break;
		}
		return statusUpdateLine;
	}
}