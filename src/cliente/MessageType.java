package cliente;
/**
 * MessageType.java
 * Sustituye el enum del tipo de mensaje que hab�a y
 * utiliza shorts para facilitar el acceso a los valores.
 * 
 * Se usa un getter para obtener el valor a partir de las strings
 * que representan cada tipo y facilitar el proceso de serializaci�n.
 * @author Daniel I�igo
 * @author Hector Martinez
 */
public class MessageType {
	// Codigos del tipo de mensaje
	public final static byte
	MSG=0x01,
	JOIN=0x02,
	LEAVE=0x03,
	NICK= 0x04,
	QUIT= 0x05,
	LIST= 0x10,
	WHO= 0x11,
	HELLO=0x20,
	OTHER=0x00;

	/**
	 * Devuelve la representación en string del tipo.
	 * 
	 * @param name
	 * @return
	 */
	public static byte getValue(String name){
		if(name.equals("MSG")) 			return MSG;
		else if(name.equals("JOIN")) 	return JOIN;
		else if(name.equals("LEAVE")) 	return LEAVE;
		else if(name.equals("NICK")) 	return NICK;
		else if(name.equals("QUIT")) 	return QUIT;
		else if(name.equals("LIST")) 	return LIST;
		else if(name.equals("WHO")) 	return WHO;
		else if(name.equals("HELLO")) 	return HELLO;
		else if(name.equals("OTHER")) 	return OTHER;
		else 							return OTHER;
	}
}
