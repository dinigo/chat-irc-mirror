package cliente;

import java.awt.EventQueue;
import java.io.IOException;
import java.net.ConnectException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;

/**
 * ClienteChat.java
 * Lanza el cliente de chat. Recibe el nombre de usuario (nick)
 * por parámetro
 *
 * @author Daniel Iñigo
 * @author Hector Martinez
 */
public class ClienteChat{
	// Tamano maximo del buffer.
	private static final int MAX_CAPACIDAD = 15;
	// Hilos y herramientas.


	private Socket network;
	private static MessageExecutor executor;
	// Informacion del usuario del chat.
	private static UserData userData;
	// Objeto log
	private static Log log;
	private static ChatWindowBuilder window;

	public static void main(String[] args) {
		// Define los valores por defecto
		Random r = new Random();
		String nick = "guest" + r.nextInt(40);
		String serverURL= "localhost";
		int serverPort = 7777;
		int logLevel = 3;
		log = Log.getInstance();
		log.setLogLevel(logLevel);
		// Obtiene los parametros que se le pasan al programa desde linea de comandos
		switch(args.length){
		case 0:
			log.w("java ClienteChat <usuario> <host puerto>");
			log.w("No se especifico ningun nombre de usuario");
			break;
		case 1:
			nick = args[0];
			break;
		case 2:
			serverURL = args[0];
			serverPort = Integer.parseInt(args[1]);
			break;
		case 3:
			nick = args[0];
			serverURL = args[1];
			serverPort = Integer.parseInt(args[2]);
			break;
		case 4:
			nick = args[0];
			serverURL = args[1];
			serverPort = Integer.parseInt(args[2]);
			logLevel = Integer.parseInt(args[3]);
			break;
		}
		log.setLogLevel(logLevel);
		log.l("______________________________");
		log.l("USER: " + nick + "        DIR: " + serverURL + ":" + serverPort);
		// Instancia y arranca un cliente
		@SuppressWarnings("unused")
		ClienteChat cliente = new ClienteChat(nick, serverURL, serverPort);
	}

	/**
	 * Se conecta al servidor e inicia los hilos de entrada y salida de datos y red.
	 * Si falla la conexión llama a la función 'quit()' que cierra los hilos, conexiones
	 * y buffers restantes.
	 * Debe haber tantas cosas porque todas dependen de que se haya instanciado la ventana.
	 * 
	 * @param userData - Edmplea el nick que se pasa por parametro para el logueo inicial
	 */
	public ClienteChat(final String nick, final String serverURL, final int serverPort) {
		// Instancia objetos compartidos (pueden ser todos final, no se van a instanciar mas que una vez).
		final ArrayBlockingQueue<Message> bufferMensajesSalida = new ArrayBlockingQueue<Message>(MAX_CAPACIDAD);
		final ArrayBlockingQueue<String> bufferGuiSalida = new ArrayBlockingQueue<String>(MAX_CAPACIDAD);
		final ArrayBlockingQueue<Message> bufferEntrada = new ArrayBlockingQueue<Message>(MAX_CAPACIDAD);
		// Instancia clases de red
		boolean connectionFailed = false;
		BinaryProtocolReader input = null;
		BinaryProtocolWriter output = null;
		try {
			network = new Socket(serverURL, serverPort);
			input 	= new BinaryProtocolReader(network.getInputStream());
			output 	= new BinaryProtocolWriter(network.getOutputStream());
		} catch (ConnectException e) {
			log.e("no se pudo abrir la conexion");
			connectionFailed = true;
		} catch (UnknownHostException e) {
			log.e("host desconocido");
			connectionFailed = true;
		} catch (IOException e) {
			log.e("error en la I/O");
			connectionFailed = true;
		}
		
		try {
			SalidaPorRed salidaPorRed 	= new SalidaPorRed(bufferMensajesSalida, output);
			EntradaPorRed entradaPorRed = new EntradaPorRed(bufferEntrada, input);
		} catch (Exception e) {
			e.printStackTrace();
		}
		// Construye la interfaz
		EventQueue.invokeLater(new Runnable() {
			@SuppressWarnings("unused")
			public void run() {
				try {		
					window = new ChatWindowBuilder(bufferGuiSalida);
					UserData userData = new UserData(nick, serverURL, serverPort, window, network);
					// El Executor retiene una instancia instancia a esta clase para modificar los parámetros y para cerrrar.
					executor = new MessageExecutor(userData, window.getTxtChat());
					// Instancia los hilos
					EntradaDatos entradaDatos 	= new EntradaDatos(userData.getNick(), bufferMensajesSalida, bufferGuiSalida);
					SalidaDatos salidaDatos		= new SalidaDatos(bufferEntrada, executor);
					// Muestra la ventana de chat
					window.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		// Si ha fallado la conexión no tiene sentido dejar el programa funcionando
		if(connectionFailed) userData.quit();
	}
}