
package cliente;

import java.util.concurrent.ArrayBlockingQueue;

/**
 * SalidaDatos.java
 * 
 * Muestra los datos que contiene el buffer de salida por pantalla
 * 
 * @author Daniel Iñigo
 * @author Hector Martinez
 */
public class SalidaDatos extends Thread{
	private final ArrayBlockingQueue<Message> bufferEntrada;
	private final MessageExecutor executor;
	private Log log;

	/**
	 * Instancia el buffer y la clase de red
	 * @param nick 
	 */
	public SalidaDatos(ArrayBlockingQueue<Message> bufferEntrada, MessageExecutor executor){
		this.bufferEntrada 	= bufferEntrada;
		this.executor		= executor;
		this.setName("SalidaDatos");
		this.log = Log.getInstance();
		this.start();
	}

	/**
	 * Lee comandos del buffer y los imprime si son mensajes.
	 */
	@Override
	public void run() {
		boolean isClosed = false;
		try {
			while(!isClosed){
				Message incomingMessage = bufferEntrada.take();
				executor.execute(incomingMessage);
				if( incomingMessage.getType() == MessageType.QUIT){
					log.l("CERRADO " + this.getName());
					this.interrupt();
					isClosed = true;
				}
			}
		} catch (InterruptedException e) {
			log.w("INTERRUMPIDO " + this.getName());
			return;
		}
	}
}
