package cliente;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;

import org.junit.Test;

public class MessageSerializerTest {

	@Test
	public void serializeTest() throws IOException {
		byte[] joinMessageBytes = new byte[]{
				(byte)0x02, (byte)0x11, (byte)0x00, (byte)0x1a, (byte)0x00,
				(byte)0x02, (byte)0x00, (byte)0x07, (byte)0x45, (byte)0x73,
				(byte)0x70, (byte)0x61, (byte)0xc3, (byte)0xb1, (byte)0x61, 
				(byte)0x00, (byte)0x0f, (byte)0x4a, (byte)0x75, (byte)0x61, 
				(byte)0x6e, (byte)0x3b, (byte)0x41, (byte)0x6e, (byte)0x61, 
				(byte)0x3b, (byte)0x49, (byte)0xc3, (byte)0xb1, (byte)0x69, 
				(byte)0x67, (byte)0x6f
		};
		DataInputStream input = new DataInputStream(new ByteArrayInputStream(joinMessageBytes));
		BinaryProtocolReader reader = new BinaryProtocolReader(input);
		Message respuesta = reader.readMessage();
		//		System.out.println(respuesta);

		byte[] respuestaBytes = MessageSerializer.serialize(respuesta);
		System.out.println(Utils.bytes2string(joinMessageBytes));
		System.out.println(Utils.bytes2string(respuestaBytes));

		assertEquals(respuestaBytes, joinMessageBytes);

	}

	@Test
	public void serializeFromMessageTest() throws IOException {

		Message respuesta = new Message((byte) 0x11,(byte) 0x02, new String[]{"España", "Juan;Ana;Iñigo"});

		byte[] respuestaBytes = MessageSerializer.serialize(respuesta);

		DataInputStream input2 = new DataInputStream(new ByteArrayInputStream(respuestaBytes));
		BinaryProtocolReader reader2 = new BinaryProtocolReader(input2);
		Message respuesta2 = reader2.readMessage();
		System.out.println(respuesta);
		System.out.println(respuesta2);
		assertEquals(respuesta.toString(), respuesta2.toString());
	}


	@Test
	public void serializeQuitTest() throws IOException {


		// Mensaje que no lleva parámetros
		byte[] joinMessageBytes = new byte[]{
				0x00, 0x05, // tipo de mensaje (QUIT)
				0x00, 0x00}; 
		DataInputStream input = new DataInputStream(new ByteArrayInputStream(joinMessageBytes));
		BinaryProtocolReader reader = new BinaryProtocolReader(input);
		Message respuesta = reader.readMessage();
					System.out.println(respuesta);
		Message m = new Message((byte) 0x05, (byte) 0x00, null);
		byte[] respuestaBytes = MessageSerializer.serialize(respuesta);
		byte[] mBytes = MessageSerializer.serialize(m);

		for (int i = 0; i < joinMessageBytes.length; i++) {
			if( respuestaBytes[i] != joinMessageBytes[i]) fail("diferetnes");
		}
		for (int i = 0; i < joinMessageBytes.length; i++) {
			if( respuestaBytes[i] != mBytes[i]) fail("diferetnes");
		}


	}













}
