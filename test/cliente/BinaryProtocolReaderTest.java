package cliente;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;

import org.junit.Test;


public class BinaryProtocolReaderTest {

	@Test
	public void readRespuestaTest() throws IOException{
		byte[] joinMessageBytes = new byte[]{
				(byte)0x02, (byte)0x11, (byte)0x00, (byte)0x1a, (byte)0x00,
				(byte)0x02, (byte)0x00, (byte)0x07, (byte)0x45, (byte)0x73,
				(byte)0x70, (byte)0x61, (byte)0xc3, (byte)0xb1, (byte)0x61, 
				(byte)0x00, (byte)0x0f, (byte)0x4a, (byte)0x75, (byte)0x61, 
				(byte)0x6e, (byte)0x3b, (byte)0x41, (byte)0x6e, (byte)0x61, 
				(byte)0x3b, (byte)0x49, (byte)0xc3, (byte)0xb1, (byte)0x69, 
				(byte)0x67, (byte)0x6f
		};
		DataInputStream input = new DataInputStream(new ByteArrayInputStream(joinMessageBytes));
		BinaryProtocolReader reader = new BinaryProtocolReader(input);
		Message respuesta;
		respuesta = reader.readMessage();
		String desiredMessage = "Type:0x11   Status:0x02"
				+ "\n    España"
				+ "\n    Juan;Ana;Iñigo";
		assertEquals(respuesta.toString(),desiredMessage);
	}
}
