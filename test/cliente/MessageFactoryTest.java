package cliente;

import static org.junit.Assert.*;

import org.junit.Test;

public class MessageFactoryTest {

	@Test
	public void createWithParamsTest(){
		String line = "/MSG España;Esto es un mensaje";
		Message message = MessageFactory.create(line);
		String desiredMessage = "Type:0x01   Status:0x00"
				+ "\n    España"
				+ "\n    Esto es un mensaje";
		assertEquals(message.toString(), desiredMessage);
	}
	
	@Test
	public void createWithoutParamsTest(){
		String line = "/WHO";
		Message message = MessageFactory.create(line);
		String desiredMessage = "Type:0x11   Status:0x00";
		assertEquals(message.toString(), desiredMessage);
	}
}